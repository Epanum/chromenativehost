## Description

Chrome native messaging host written in go

input json format:
```json
{
    "commands": []
}
```
each command gets executed in there own go rutine, 
when all commands are done, `{"result":"executed"}` is returned. 

## Install
* build `main.go`
* insert valid chrome extension/app key in `com.example.nativehost.json`
* run `install_host.bat`


## Use

Call from chrome extension/app

Example:
```javascript
var data = {
    commands: []
};
   
data.commands.push("executable command");
    
chrome.runtime.sendNativeMessage('com.example.nativehost',
    data,
function(response) {
    console.log(JSON.stringify(response));
});
```
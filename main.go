package main

import (
    "bufio"
    "os"
    "io"
	"sync"
	"strings"
	"os/exec"
	"encoding/json"
	"bytes"
	"encoding/binary"
)

type jObject struct {
    Commands []string
}

func check(e error) {
    if e != nil {
        panic(e)
    }
}

// Converts binary int32 representation to int32
func readInt32(data []byte) (ret int32) {
    buf := bytes.NewBuffer(data)
    binary.Read(buf, binary.LittleEndian, &ret)
    return
}

// Converts int32 to binary int32 representation
func writeInt32(data int) (ret []byte) {
    buf := new(bytes.Buffer)
    binary.Write(buf, binary.LittleEndian, data)    
    ret = buf.Bytes()    
    return
}

// Split indput in command and arguments and executed
// Calls done on waitgroup to signal gorutine end.
func exeCmd(cmd string, wg *sync.WaitGroup) {
  parts := strings.Fields(cmd)
  head := parts[0]
  parts = parts[1:len(parts)]

  _ , err := exec.Command(head,parts...).Output()
  check(err)

  wg.Done()
}

func main() {
    reader := bufio.NewReader(os.Stdin)
    
    // Read the first 4 bytes (indicates length of message to follow)
    l := make([]byte, 4)
    _ , err := io.ReadFull(reader, l)
    check(err)
    length := readInt32(l)

    // Read length indicated by first 4 bytes
    message := make([]byte, length)
    _ , err1 := io.ReadFull(reader, message)
    check(err1)
       
    res := jObject{}
    json.Unmarshal(message, &res)
    
    wg := new(sync.WaitGroup)            
    for index := 0; index < len(res.Commands); index++ {
        wg.Add(1)
        go exeCmd(res.Commands[index], wg)        
    }
    wg.Wait()
    
    writer := bufio.NewWriter(os.Stdout)    
    defer writer.Flush()
 
    d2 := []byte("{\"result\":\"executed\"}")
    writer.Write(writeInt32(len(d2)))
    writer.Write(d2)
}